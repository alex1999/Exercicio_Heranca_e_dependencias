/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicio;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Spinner;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class FXMLMenuController implements Initializable {

    @FXML
    private TextField nomeLabel;
    @FXML
    private TextField endeLabel;
    @FXML
    private Spinner<Pessoa> idadLabel;
    @FXML
    private RadioButton alunoRadio;
    @FXML
    private TextField semLabel;
    @FXML
    private TextField cursoLabel;
    @FXML
    private RadioButton funcRadio;
    @FXML
    private TextField salLabel;
    @FXML
    private RadioButton profRadio;
    @FXML
    private TextField discLabel;
    @FXML
    private TextField funcLabel;
    @FXML
    private TextField setoLabel;
    @FXML
    private TableView<Pessoa> tabela;
        
        @FXML
        private TableColumn<Pessoa, String> nomeTab;
    
        @FXML
        private TableColumn<Pessoa, Integer> idadeTab;
        
        @FXML
        private TableColumn<Pessoa, String> enderTabela;
        
        @FXML
        private TableColumn<Pessoa, String> funcTab;
    
    @FXML
    private Button castroBot;
    @FXML
    private RadioButton admRadio;
       
    /**
     *
     */
    @FXML
    public void selectedAluno(){
        this.alunoRadio.setDisable(false);
        this.funcRadio.setDisable(true);
    }
    @FXML
    public void selectedFunc(){
        this.alunoRadio.setDisable(true);
        this.funcRadio.setDisable(false);
    }
    @FXML
    public void selectedProf(){
        this.alunoRadio.setDisable(true);
        this.funcRadio.setDisable(true);
        this.profRadio.setDisable(false);
        this.admRadio.setDisable(true);
    }
    @FXML
     public void selectedAdm(){
        this.alunoRadio.setDisable(true);
        this.funcRadio.setDisable(true);
        this.profRadio.setDisable(true);
        this.admRadio.setDisable(false);
    }
    
    
     
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }   
    
    @FXML
    private void cadastrar(){
    
        Pessoa p = new Pessoa();
        
        if(this.alunoRadio.isDisabled()){
            
        }
      
    
    }
    
    public void detalhes(){}

    public void deletar(){}
    
    public void editar(){}
    
}
